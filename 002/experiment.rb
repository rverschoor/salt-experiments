#!/usr/bin/env ruby

require '../../salt/app/salt'

salt = Salt.new
salt.gitlab '13.5.4'

salt.time_machine '2019-12-15'

salt.add_license '../../salt-licenses/20191215-20201215-l10p10t0.gitlab-license'

# Create 10 users
# There are 11 users, the 10 we created + root
salt.create_users '01'..'10'

# Make a screenshot of the Admin panel
salt.screenshot 'http://localhost/admin', name: 'screen1'

# Run the historical maximum user counter task
salt.kick_muc

# Make a screenshot of the Admin panel
salt.screenshot 'http://localhost/admin', name: 'screen2'

# Delete one user
salt.delete_user '10'

# Run the historical maximum user counter task
salt.kick_muc

# Make a screenshot of the Admin panel
salt.screenshot 'http://localhost/admin', name: 'screen3'

# Stop & delete the Docker container, stop time machine
salt.bye
