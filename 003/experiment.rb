#!/usr/bin/env ruby

require '../../salt/app/salt'

versions = %w[11.0.0 12.0.0 13.0.0]

versions.each do |version|
  salt = Salt.new
  salt.gitlab "#{version}"
  salt.time_machine '2019-12-15'
  salt.add_license '../../salt-licenses/20191215-20201215-l10p10t0.gitlab-license'
  salt.create_users '02'..'13'
  salt.screenshot 'http://localhost/admin', name: "screen-#{version}-pre"
  salt.kick_muc
  salt.delete_users '11'..'13'
  salt.kick_muc
  salt.screenshot 'http://localhost/admin', name: "screen-#{version}"
  salt.bye
end
