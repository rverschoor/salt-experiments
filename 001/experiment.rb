#!/usr/bin/env ruby

require '../../salt/app/salt'

salt = Salt.new
salt.gitlab '13.5.4'
salt.screenshot 'http://localhost/admin', name: 'screen1'
salt.stop
salt.delete
