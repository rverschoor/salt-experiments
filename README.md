## Experiments

- 001 - Proof of concept \
Start a GitLab instance and make a screenshot of the admin panel.
- 002 - Proof of concept with time machine \
  Demonstrate license overage.
- 003 - Show Admin screen for different GitLab versions 

## Available licenses for testing:

All test licenses are Starter, unless name indicates otherwise.

Initial license:
- 20181215-20191215-l10p0t0
- 20181215-20191215-l12p0t0

Renewal with 2 week early startdate:
- 20191201-20201215-l10p10t0
- 20191201-20201215-l10p10t2
- 20191201-20201215-l10p12t0

Renewal:
- 20191215-20201215-l10p10t0
- 20191215-20201215-l10p10t2
- 20191215-20201215-l10p12t0

Trial:

- 20191215-20191222-tl10p0t0
- 20191215-20191222-tl10p0t2
